package io.androidpro.architect

import android.app.Application
import io.androidpro.architect.di.AppModule
import io.androidpro.architect.di.AppModuleImpl

class MainApp : Application() {
    companion object {
        lateinit var appModule: AppModule
    }

    override fun onCreate() {
        super.onCreate()
        appModule = AppModuleImpl(this)
    }
}