package io.androidpro.architect.utils

sealed class AppError(
    override val message: String?,
    override val cause: Throwable?
) : Throwable(message, cause) {
    object Unknown : AppError("0", null) {
        private fun readResolve(): Any = Unknown
    }

    data class NoOrderFound(val orderId: String, override val message: String?) :
        AppError(message, null)

    data class NetworkError(
        val httpCode: Int,
        val code: String,
        override val message: String?
    ) : AppError(message, null)
}