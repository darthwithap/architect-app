package io.androidpro.architect.utils

sealed class AppResult<T> {
    data class Success<T>(val data: T) : AppResult<T>()
    data class Error<T>(val error: AppError): AppResult<T>()
}