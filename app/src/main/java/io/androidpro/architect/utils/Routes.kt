package io.androidpro.architect.utils

object Routes {
    const val ORDER_LIST = "order_list_screen"
    const val ORDER_DETAILS = "order_details_screen"
}