package io.androidpro.architect.di

import android.content.Context
import io.androidpro.architect.data.order_list.FakeRepository
import io.androidpro.architect.data.order_list.Repository

interface AppModule {
    val repository: Repository
}

class AppModuleImpl(
    val appContext: Context
) : AppModule {
    override val repository: Repository
        get() = FakeRepository
}