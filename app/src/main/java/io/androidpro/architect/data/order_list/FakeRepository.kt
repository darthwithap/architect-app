package io.androidpro.architect.data.order_list

import io.androidpro.architect.utils.AppError
import io.androidpro.architect.utils.AppResult
import kotlinx.coroutines.delay
import kotlin.math.floor
import kotlin.random.Random

/**
 * Created by jaroslawmichalik on 02/10/2023
 */
object FakeRepository : Repository {

    private val orders = List(99) { i ->
        val orderId = i.toString()
        val customerName = "Customer $i"
        val customerAddress = "Address $i"
        val customerContact = "Contact $i"
        val restaurantName = "Restaurant ${Random.nextInt(1, 5)}"
        val restaurantAddress = "Restaurant Address ${Random.nextInt(1, 5)}"
        val orderItems = generateFakeOrderItems(Random.nextInt(1, 5))
        val specialInstructions = if (Random.nextBoolean()) "Special Instructions $i" else null
        val orderStatus = OrderStatus.values()[Random.nextInt(OrderStatus.values().size)]
        val timestamp = System.currentTimeMillis()

        Order(
            orderId,
            customerName,
            customerAddress,
            customerContact,
            restaurantName,
            restaurantAddress,
            orderItems,
            specialInstructions,
            orderStatus,
            timestamp
        )
    }

    override suspend fun getAll(): AppResult<List<Order>> {
        delay(700)
        // Just to send a random error once a while as fake error
        if (floor(Math.random() * 21).toInt() == 14) {
            return AppResult.Error(AppError.Unknown)
        }
        return AppResult.Success(orders)
    }

    override suspend fun getById(id: String): AppResult<Order> {
        delay(300)
        if (orders.find { it.orderId == id } == null) {
            return AppResult.Error(AppError.NoOrderFound(id, "No order found with id: $id"))
        }
        return AppResult.Success(orders.first { it.orderId == id })
    }

    private fun generateFakeOrderItems(numberOfItems: Int): List<OrderItem> {
        val random = Random

        return (1..numberOfItems).map { i ->
            val itemName = "Item $i"
            val quantity = random.nextInt(1, 5)
            val price = random.nextDouble(5.0, 20.0)

            OrderItem(itemName, quantity, price)
        }
    }
}
