package io.androidpro.architect.data.order_list

import io.androidpro.architect.data.order_list.Order
import io.androidpro.architect.utils.AppResult

interface Repository {
    suspend fun getAll(): AppResult<List<Order>>
    suspend fun getById(id: String): AppResult<Order>
}