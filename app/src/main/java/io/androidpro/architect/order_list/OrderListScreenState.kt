package io.androidpro.architect.order_list

import io.androidpro.architect.utils.AppError
import io.androidpro.architect.data.order_list.Order

data class OrderListScreenState(
    val orders: List<Order>,
    val detailedOrder: Order?,
    val error: AppError?,
    val isLoading: Boolean
) {
    fun isError() = error != null
    fun hasNoOrders() = orders.isEmpty()
    fun hasOrders() = orders.isNotEmpty()

    companion object {
        val INITIAL = OrderListScreenState(
            orders = emptyList(),
            detailedOrder = null,
            error = null,
            isLoading = true
        )
    }
}
