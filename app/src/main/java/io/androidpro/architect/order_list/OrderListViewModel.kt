package io.androidpro.architect.order_list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.androidpro.architect.data.order_list.Repository
import io.androidpro.architect.utils.AppResult
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class OrderListViewModel(
    private val repository: Repository
) : ViewModel() {

    private var _state = MutableStateFlow(OrderListScreenState.INITIAL)
    val state: StateFlow<OrderListScreenState> = _state.asStateFlow()

    init {
        refreshOrders()
    }

    private fun refreshOrders() {
        viewModelScope.launch {
            when (val result = repository.getAll()) {
                is AppResult.Error -> {
                    _state.value = _state.value.copy(
                        error = result.error,
                        isLoading = false
                    )
                }

                is AppResult.Success -> {
                    _state.value = _state.value.copy(
                        orders = result.data,
                        isLoading = false
                    )
                }
            }
        }
    }

    fun getOrderDetails(orderId: String) {
        _state.value = _state.value.copy(isLoading = true)
        viewModelScope.launch {
            when (val result = repository.getById(orderId)) {
                is AppResult.Error -> {
                    _state.value = _state.value.copy(
                        error = result.error,
                        isLoading = false
                    )
                }

                is AppResult.Success -> {
                    _state.value = _state.value.copy(
                        detailedOrder = result.data,
                        isLoading = false
                    )
                }
            }
        }
    }

    fun onReturnFromOrderDetails() {
        _state.value = _state.value.copy(detailedOrder = null)
    }

    fun onSwipeToRefresh() {
        _state.value = _state.value.copy(isLoading = true)
        refreshOrders()
    }

}