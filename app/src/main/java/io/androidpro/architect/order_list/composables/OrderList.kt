package io.androidpro.architect.order_list.composables

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import io.androidpro.architect.order_list.OrderListScreenState

@Composable
fun OrderList(
    padding: PaddingValues,
    state: OrderListScreenState,
    onOrderClick: (orderId: String) -> Unit
) {
    when {
        state.hasOrders() -> {
            LazyColumn(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(padding),
                verticalArrangement = Arrangement.spacedBy(16.dp)
            ) {
                items(state.orders) { order ->
                    OrderItemCard(order) {
                        onOrderClick(it)
                    }
                }
            }
        }

        state.hasNoOrders() -> {
            Box(modifier = Modifier
                .fillMaxSize()
                .padding(padding)) {
                Text(
                    "No orders found :(",
                    modifier = Modifier.align(Alignment.Center)
                )
            }
        }
    }
}