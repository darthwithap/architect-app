package io.androidpro.architect.order_list

import android.widget.Toast
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import io.androidpro.architect.MainApp
import io.androidpro.architect.order_list.composables.OrderDetails
import io.androidpro.architect.order_list.composables.OrderList
import io.androidpro.architect.utils.Routes

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun OrderListScreen(
    navController: NavHostController
) {
    val viewModel: OrderListViewModel =
        viewModel<OrderListViewModel>(factory = viewModelsFactory {
            OrderListViewModel(MainApp.appModule.repository)
        })
    val state: OrderListScreenState by viewModel.state.collectAsState()
    val context = LocalContext.current

    LaunchedEffect(key1 = state.isError()) {
        if (state.isError()) {
            Toast.makeText(context, state.error!!.message, Toast.LENGTH_SHORT).show()
        }
    }
    Scaffold(
        topBar = {
            TopAppBar(title = { Text("Marketplace") })
        }
    ) { padding ->
        NavHost(navController = navController, startDestination = Routes.ORDER_LIST) {
            composable(route = Routes.ORDER_LIST) {
                OrderList(padding, state) { orderId ->
                    navController.navigate(Routes.ORDER_DETAILS + "/$orderId")
                }
            }
            composable(
                route = Routes.ORDER_DETAILS + "/{orderId}",
                arguments = listOf(navArgument("orderId") {
                    type = NavType.StringType
                })
            ) {
                val orderId = it.arguments?.getString("orderId") ?: ""
                viewModel.getOrderDetails(orderId)
                OrderDetails(state.detailedOrder, padding) {
                    navController.popBackStack()
                    viewModel.onReturnFromOrderDetails()
                }
            }
        }
    }
}